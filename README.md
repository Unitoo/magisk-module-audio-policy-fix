# Introduction

This module applies a fix for the audio policy inside /vendor/etc.
More info on: https://gitlab.com/LineageOS/issues/android/-/issues/1805

# Installation

## What you need

* Magisk v19+


## Procedure

Simply create a zip folder with whatyouwant name and put all content inside.
Later open Magisk application and install module from device.

Done
